import sys
import re
path = sys.argv[1]
open_file = open(path,"r")
pattern = "^real[\s]*[0-9]{1}m[0-9]+.[0-9]+s"
for line in open_file:
    result = re.match(pattern, line)
    if result:
        line = line.replace("real","")
        line = line.replace(",",".")
        line = line.replace(" ","")
        line = line.replace("m","")
        line = line.replace("s","")
        line = line.replace("\t","")
        print(line, end="")