// **********************************************************************************
//
// BSD License.
// This file is part of a canny edge detection implementation.
//
// Copyright (c) 2017, Bruno Keymolen, email: bruno.keymolen@gmail.com
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// Redistributions in binary form must reproduce the above copyright notice,
// this
// list of conditions and the following disclaimer in the documentation and/or
// other
// materials provided with the distribution.
// Neither the name of "Bruno Keymolen" nor the names of its contributors may be
// used to endorse or promote products derived from this software without
// specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// **********************************************************************************
#include <dirent.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <map>
#include <string>
#include <omp.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "canny.hpp"

#define NUM_IMAGENS 103

extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;

std::string img_path;

int low_threshold = 30;
int high_threshold = 100;

const char* CW_IMG_ORIGINAL = "Original";
const char* CW_IMG_GRAY = "Grayscale";
const char* CW_IMG_EDGE = "Canny Edge Detection";

void doTransform(std::string, int num_task);

void usage(char* s) {
    fprintf(stderr, "\n");
    fprintf(stderr, "canny edge detection. build: %s-%s \n", __DATE__,
            __TIME__);
    fprintf(stderr,
            "%s -s <source file> [-l <low threshold>] [-h <high threshold>] "
            "[-? help]",
            s);
    fprintf(stderr, "   s: path image file\n");
    fprintf(stderr, "   l: canny low threshold\n");
    fprintf(stderr, "   h: canny high threshold\n");
    fprintf(stderr,
            "\nexample:  ./canny -s img/Valve_original.PNG "
            "-l 30 -h 90\n");
    fprintf(stderr, "\n");
}

std::string imagens_teste[NUM_IMAGENS] = {
    "img/45deg.jpg",
    "img/fence.png",
    "img/gaus.png",
    "img/horizontal.png",
    "img/russell-crowe-robin-hood-arrow.jpg",
    "img/United_States_money_coins.jpg",
    "img/Valve_original.PNG",
    "img/vertical.png",
    "img/mickey.JPG",
    "img/mickey1.jpg",
    "img/robson.jpeg",
    "img/dogromantico.jpeg",
    "img/eu.jpeg",
    "img/eu2.jpeg",
    "img/fabinho.jpeg",
    "img/familia.jpeg",
    "img/jao.jpeg",
    "img/patodela.jpeg",
    "img/dogfabinho.jpeg",
    "img/coritiba.jpg",
    "img/eucommtalface.jpeg",
    "img/gatochorando.jpeg",
    "img/maocortada.jpeg",
    "img/mike.jpeg",
    "img/boi.jpg",
    "img/jhoser.jpeg",
    "img/arvores.jpeg",
    "img/biggercity.jpeg",
    "img/cestabola.jpeg",
    "img/city.jpeg",
    "img/cityier.jpeg",
    "img/jojo.jpeg",
    "img/lapz.jpeg",
    "img/nottigre.jpeg",
    "img/onda.jpeg",
    "img/oraoraoraoraoraoraoraora.jpeg",
    "img/pebola.jpeg",
    "img/pexe.jpeg",
    "img/pps.jpeg",
    "img/preda.jpeg",
    "img/saddoge.jpeg",
    "img/sadfogo.jpeg",
    "img/serjao.jpeg",
    "img/stadio.jpeg",
    "img/trige.jpeg",
    "img/ventu.jpeg",
    "img/vila.jpeg",
    "img/zebra.jpeg",
    "img/zebrax2.jpeg",
    "img/igreja.jpg",
    "img/igreja2.jpg",
    "img/animalfofo.jpeg",
    "img/cachorroebolas.jpeg",
    "img/catsleep.jpeg",
    "img/chapeudecat.jpeg",
    "img/cone.jpeg",
    "img/dogemelancia.jpeg",
    "img/dogfeliz.jpeg",
    "img/doguinho.jpeg",
    "img/fucinho.jpeg",
    "img/gatonolixo.jpeg",
    "img/gorduxos.jpeg",
    "img/jaquin.jpeg",
    "img/johnmaclane.jpeg",
    "img/lisa.jpeg",
    "img/lulamolusco.jpeg",
    "img/otakufedido.jpeg",
    "img/patinho.jpeg",
    "img/porquinho.jpeg",
    "img/rapeizeboa.jpeg",
    "img/robsonkid.jpeg",
    "img/calabocadatebayo.jpeg",
    "img/elefante.jpeg",
    "img/familia2.jpeg",
    "img/familia3.jpeg",
    "img/gatinhos.jpeg",
    "img/gatonamesa.jpeg",
    "img/girafa.jpeg",
    "img/guigo.jpeg",
    "img/pequenaeindependente.jpeg",
    "img/rey.jpeg",
    "img/uau.jpeg",
    "img/thanos.png",
    "img/picapau.jpg",
    "img/lulamolusco2.jpg",
    "img/caverna.jpg",
    "img/cristoredentor.jpg",
    "img/meninoney.jpg",
    "img/ufpr.jpg",
    "img/ufpr2.jpeg",
    "img/ufpr3.png",
    "img/mundo.png",
    "img/cr7.jpeg",
    "img/dickleberg.jpg",
    "img/faustao.jpeg",
    "img/faustao.jpg",
    "img/fofoesquecionome.jpeg",
    "img/jubuti.jpeg",
    "img/mario.jpg",
    "img/messikk.jpg",
    "img/mario.jpg",
    "img/superpoderosas.jpg",
    "img/scooby.jpg",
};
  
int main(int argc, char** argv) {
    low_threshold = 30;
    high_threshold = 90;
    #ifdef full_parallel
        int num_tasks_inside = atoi(argv[1]);
        int num_tasks_outside = num_tasks_inside;
	#pragma omp parallel for num_threads(num_tasks_outside)
    #endif
    #ifdef out_parallel
        int num_tasks_inside = 1;
	    int num_tasks_outside = atoi(argv[1]);
        #pragma omp parallel for num_threads(num_tasks_outside)
    #endif
    #ifdef inside_parallel
        int num_tasks_inside = atoi(argv[1]);
        int num_tasks_outside = 1;
	#pragma omp parallel for num_threads(num_tasks_outside)
    #endif

    #ifdef serial
        int num_tasks_inside = 1;
    #endif
    for(int i = 0; i < NUM_IMAGENS; ++i){
        printf("task number %d applying Canny Algorithm\n",omp_get_thread_num());
        doTransform(imagens_teste[i], num_tasks_inside);
    }
    return 0;
}

void doTransform(std::string file_path, int num_task) {
    cv::Mat img_edge;
    cv::Mat img_gray;

    cv::Mat img_ori = cv::imread(file_path, 1);
    cv::cvtColor(img_ori, img_gray, cv::COLOR_BGR2GRAY);

    int w = img_gray.cols;
    int h = img_ori.rows;

    keymolen::Canny::NoiseFilter filter = keymolen::Canny::NoiseFilter::Gaus3x3;

    std::string arquivo_resultado = "results/" + file_path;

    while (1) {
        cv::Mat img_edge(h, w, CV_8UC1, cv::Scalar::all(0));

        keymolen::Canny canny(w, h);
        canny.edges(img_edge.data, img_gray.data, filter, low_threshold,
                    high_threshold, num_task);
        cv::imwrite(arquivo_resultado, img_edge);
        break;
    }
    return;
}
