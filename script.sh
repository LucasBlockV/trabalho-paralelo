for type in "full_parallel" "out_parallel" "inside_parallel"  
do
    make $type
    for (( i = 1; i <= 6; i++ )) ### number of tasks ###
    do
        touch "time_$type_$i.txt"
        touch "dados_$type_$i.txt"
        for (( j = 1 ; j <= 10; j++ )) ### 10 tests ###
        do
            echo "Rodando para $i tasks e a vez de numero $j"
            { time ./canny $i;}  2> "time_$type_$i.txt"
            python3 getRealTime.py "time_$type_$i.txt" >> "dados_$type_$i.txt"
            rm time_$type_$i.txt
        done

    done
    touch "gnu_plot_$type.gnu"
    echo "set grid" >> "gnu_plot_$type.gnu"
    echo "set ylabel 'tempo'" >> "gnu_plot_$type.gnu"
    echo "set xlabel 'Número do teste'" >> "gnu_plot_$type.gnu"
    echo "set title 'Tempo por N° de tasks '" >> "gnu_plot_$type.gnu"
    echo "set key above" >> "gnu_plot_$type.gnu"
    echo "set terminal png" >> "gnu_plot_$type.gnu"
    echo "set output '$type.png'" >> "gnu_plot_$type.gnu"
    echo "plot 'dados_$type_""1.txt' title '1 Threads' with lines, 'dados_$type_""2.txt' title '2 Threads' with lines, 'dados_$type_""3.txt' title '3 Threads' with lines, 'dados_$type_""4.txt' title '4 Threads' with lines, 'dados_$type_""5.txt' title '5 Threads' with lines,  'dados_$type_""6.txt' title '6 Threads' with lines" >> "gnu_plot_$type.gnu" 

    gnuplot -e "load 'gnu_plot_$type.gnu'"
 #   xdg-open "$type.png"
    make clean
done
make clean
make serial
touch "time_serial.txt"
touch "dados_serial.txt"
for (( j = 1 ; j <= 10; j++ )) ### 10 tests ###
do
    echo "Rodando para serial e a vez de numero $j"
    { time ./canny 1;}  2> "time_serial.txt"
    python3 getRealTime.py "time_serial.txt" >> "dados_serial.txt"
    rm time_serial.txt
done
touch "gnu_plot_serial.gnu"
echo "set grid" >> "gnu_plot_serial.gnu"
echo "set ylabel 'Tempo'" >> "gnu_plot_serial.gnu"
echo "set xlabel 'Número do teste'" >> "gnu_plot_serial.gnu"
echo "set title 'Tempo por N° de tasks '" >> "gnu_plot_serial.gnu"
echo "set key above" >> "gnu_plot_serial.gnu"
echo "set terminal png" >> "gnu_plot_serial.gnu"
echo "set output 'serial.png'" >> "gnu_plot_serial.gnu"
echo "plot 'dados_serial.txt' title 'Serial' with lines" >> "gnu_plot_serial.gnu"
gnuplot -e "load 'gnu_plot_serial.gnu'"
#xdg-open "serial.png"
